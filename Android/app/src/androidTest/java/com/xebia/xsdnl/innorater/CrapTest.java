/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;

import static org.hamcrest.Matchers.hasToString;
import static org.junit.Assert.assertThat;

public class CrapTest {

    @Test
    @SmallTest
    public void testCharSequenceTrimming() {
        assertThat(Crap.trim(""), hasToString(""));
        assertThat(Crap.trim(" "), hasToString(""));
        assertThat(Crap.trim(" a"), hasToString("a"));
        assertThat(Crap.trim("a "), hasToString("a"));
        assertThat(Crap.trim(" a "), hasToString("a"));
        assertThat(Crap.trim(" a b "), hasToString("a b"));
    }

}
