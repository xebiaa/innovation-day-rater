/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater;

import android.preference.PreferenceManager;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.espresso.util.HumanReadables;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;

import com.xebia.xsdnl.innorater.discovery.DiscoverySystem;
import com.xebia.xsdnl.innorater.host.SessionHostActivity;
import com.xebia.xsdnl.innorater.spectator.SessionSelectionActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.assertNoUnverifiedIntents;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Exercises the MainActivity.
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> mMainActRule = new IntentsTestRule<>(MainActivity.class);

    @Before
    public void disableSessionDiscovery() {
        DiscoverySystem.getInstance(mMainActRule.getActivity()).setEnabled(false);
    }

    @Test
    public void discoveryEnabledSwitchShouldUpdateSharedPreferences() throws InterruptedException {
        assertThat(DiscoverySystem.getInstance(mMainActRule.getActivity()).isEnabled(), is(false));

        onView(withId(R.id.main__nearbySwitch)).perform(click());
        Thread.sleep(25L);
        assertThat(DiscoverySystem.getInstance(mMainActRule.getActivity()).isEnabled(), is(true));
        assertThat(PreferenceManager.getDefaultSharedPreferences(mMainActRule.getActivity())
                .getBoolean("DiscoveryEnabled", false), is(true));

        onView(withId(R.id.main__nearbySwitch)).perform(click());
        Thread.sleep(25L);
        assertThat(DiscoverySystem.getInstance(mMainActRule.getActivity()).isEnabled(), is(false));
        assertThat(PreferenceManager.getDefaultSharedPreferences(mMainActRule.getActivity())
                .getBoolean("DiscoveryEnabled", true), is(false));
    }

    @Test
    public void tappingSpectateShouldLaunchSelectionActivity() {
        onView(withId(R.id.main__spectatorStart)).perform(click());
        intended(hasComponent(SessionSelectionActivity.class.getName()));
        assertNoUnverifiedIntents();
    }

    @Test
    public void tappingHostShouldRevealHostInputFieldsAndTappingItAgainShouldLaunchHostActivity() {
        onView(withId(R.id.main__hostSessionName)).check(isNotShown());
        onView(withId(R.id.main__hostTalkName)).check(isNotShown());
        onView(withId(R.id.main__hostStart)).check(isEnabled());
        onView(withId(R.id.main__hostStart)).perform(click());

        onView(withId(R.id.main__hostSessionName)).check(isShown());
        onView(withId(R.id.main__hostTalkName)).check(isShown());
        onView(withId(R.id.main__hostStart)).check(isDisabled());

        onView(withId(R.id.main__hostSessionName)).perform(
                ViewActions.clearText(),
                ViewActions.typeText("Test session"));
        onView(withId(R.id.main__hostTalkName)).perform(
                ViewActions.clearText(),
                ViewActions.typeText("Test talk"));
        onView(withId(R.id.main__hostStart)).check(isEnabled());
        onView(withId(R.id.main__hostStart)).perform(click());

        intended(allOf(
                isInternal(),
                hasComponent(SessionHostActivity.class.getName()),
                hasExtra("SessionName", "Test session"),
                hasExtra("TalkName", "Test talk")
        ));
        assertNoUnverifiedIntents();
    }

    static ViewAssertion isNotShown() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                ViewMatchers.assertThat("View is shown, but shouldn't be: " + HumanReadables.describe(view), view.isShown(), is(false));
            }
        };
    }

    static ViewAssertion isShown() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                ViewMatchers.assertThat("View isn't shown, but should be: " + HumanReadables.describe(view), view.isShown(), is(true));
            }
        };
    }

    static ViewAssertion isDisabled() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                ViewMatchers.assertThat("View is enabled, but shouldn't be: " + HumanReadables.describe(view), view.isEnabled(), is(false));
            }
        };
    }

    static ViewAssertion isEnabled() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                ViewMatchers.assertThat("View is disabled, but shouldn't be: " + HumanReadables.describe(view), view.isEnabled(), is(true));
            }
        };
    }
}
