<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright 2015 Xebia Nederland B.V.
  ~
  ~ License:
  ~ Creative Commons Attribution-NonCommercial 4.0 International
  ~ http://creativecommons.org/licenses/by-nc/4.0/
  -->

<android.support.design.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <android.support.v4.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_behavior="@string/appbar_scrolling_view_behavior">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:paddingBottom="@dimen/activity_vertical_margin"
            android:paddingLeft="@dimen/activity_horizontal_margin"
            android:paddingRight="@dimen/activity_horizontal_margin"
            android:paddingTop="@dimen/activity_vertical_margin">

            <android.support.v7.widget.CardView style="@style/AppCard">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:id="@+id/main__spectatorCardTitle"
                        style="@style/AppCardTitle"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/main__spectator_card_title" />

                    <TextView
                        android:id="@+id/main__spectatorText"
                        style="@style/AppCardBodyText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__spectatorCardTitle"
                        android:text="@string/main__spectator_title" />

                    <Button
                        android:id="@+id/main__spectatorStart"
                        style="@style/AppCardButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__spectatorText"
                        android:layout_centerHorizontal="true"
                        android:text="@string/main__spectator_button_text" />

                </RelativeLayout>
            </android.support.v7.widget.CardView>

            <android.support.v7.widget.CardView
                style="@style/AppCard"
                android:layout_marginTop="16dp">

                <RelativeLayout
                    android:id="@+id/main__hostCardContents"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:animateLayoutChanges="true">

                    <TextView
                        android:id="@+id/main__hostCardTitle"
                        style="@style/AppCardTitle"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/main__host_card_title" />

                    <TextView
                        android:id="@+id/main__hostText"
                        style="@style/AppCardBodyText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__hostCardTitle"
                        android:labelFor="@+id/main__hostSessionName"
                        android:text="@string/main__host_card_text" />

                    <android.support.design.widget.TextInputLayout
                        android:id="@+id/main__hostSessionNameLayout"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__hostText"
                        android:layout_marginTop="8dp"
                        android:visibility="gone">

                        <EditText
                            android:id="@id/main__hostSessionName"
                            style="@style/AppCardEditText"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:hint="@string/main__host_session_name_hint"
                            android:imeOptions="actionNext|flagNoExtractUi"
                            android:inputType="text"
                            android:maxLength="@integer/session_name_length"
                            android:maxLines="1"
                            android:nextFocusDown="@+id/main__hostTalkName"
                            android:saveEnabled="true"
                            android:selectAllOnFocus="true"
                            android:singleLine="true" />
                    </android.support.design.widget.TextInputLayout>

                    <android.support.design.widget.TextInputLayout
                        android:id="@+id/main__hostTalkNameLayout"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__hostSessionNameLayout"
                        android:visibility="gone">

                        <EditText
                            android:id="@id/main__hostTalkName"
                            style="@style/AppCardEditText"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:hint="@string/main__host_talk_name_hint"
                            android:imeOptions="actionGo|flagNoExtractUi"
                            android:inputType="text"
                            android:maxLength="@integer/session_name_length"
                            android:maxLines="1"
                            android:nextFocusUp="@id/main__hostSessionName"
                            android:saveEnabled="true"
                            android:selectAllOnFocus="true"
                            android:singleLine="true"
                            android:text="@string/main__host_default_talk_title" />
                    </android.support.design.widget.TextInputLayout>

                    <Button
                        android:id="@+id/main__hostStart"
                        style="@style/AppCardButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_below="@id/main__hostTalkNameLayout"
                        android:layout_centerHorizontal="true"
                        android:text="@string/main__host_button_text" />
                </RelativeLayout>
            </android.support.v7.widget.CardView>

            <android.support.v7.widget.CardView
                style="@style/AppCard"
                android:layout_marginTop="16dp">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <android.support.v7.widget.SwitchCompat
                        android:id="@+id/main__nearbySwitch"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentStart="true"
                        android:layout_centerVertical="true"
                        android:checked="true" />

                    <ImageView
                        android:id="@+id/main__nearbyLogo"
                        android:layout_width="48dp"
                        android:layout_height="48dp"
                        android:layout_alignParentEnd="true"
                        android:layout_centerVertical="true"
                        android:contentDescription="@string/main__nearby_logo_description"
                        android:src="@drawable/logo_nearby_color_48dp" />

                    <TextView
                        android:id="@+id/main__nearbyText"
                        style="@style/AppCardBodyText"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_centerVertical="true"
                        android:layout_marginEnd="8dp"
                        android:layout_marginStart="16dp"
                        android:layout_toEndOf="@id/main__nearbySwitch"
                        android:layout_toStartOf="@id/main__nearbyLogo"
                        android:text="@string/main__nearby_title" />

                </RelativeLayout>
            </android.support.v7.widget.CardView>

        </LinearLayout>
    </android.support.v4.widget.NestedScrollView>

    <android.support.design.widget.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <android.support.v7.widget.Toolbar
            android:id="@+id/tool_bar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_scrollFlags="scroll|enterAlways" />

    </android.support.design.widget.AppBarLayout>

</android.support.design.widget.CoordinatorLayout>
