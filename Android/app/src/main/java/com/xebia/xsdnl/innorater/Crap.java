/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.widget.ImageView;

/**
 * Crap, a utility class. I had hoped to avoid that.
 */
public class Crap {

    /**
     * Trim a CharSequence, both ends.
     * @param cs a char sequence.
     * @return a trimmed sub-sequence, or the entire original thing.
     */
    public static CharSequence trim(@Nullable CharSequence cs) {
        if (cs == null || cs.length() == 0) {
            return cs;
        }
        int start = 0;
        int end = cs.length();
        for (; end > 1; end--) {
            if (!Character.isWhitespace(cs.charAt(end - 1))) {
                break;
            }
        }
        for (; start < end; start++) {
            if (!Character.isWhitespace(cs.charAt(start))) {
                break;
            }
        }
        return cs.subSequence(start, end);
    }

    public static void showNearbyStateInImageView(ImageView iv, boolean enabled) {
        if (enabled) {
            iv.setImageResource(R.drawable.logo_nearby_color_48dp);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv.setImageTintList(null);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv.setImageResource(R.drawable.ic_nearby_white_48dp);
                iv.setImageTintList(iv.getResources().getColorStateList(R.color.primary_text_disabled_light));
            } else {
                Drawable raw = iv.getResources().getDrawable(R.drawable.ic_nearby_white_48dp);
                Drawable tintable = DrawableCompat.wrap(raw);
                DrawableCompat.setTint(tintable, iv.getResources().getColor(R.color.primary_text_disabled_light));
                iv.setImageDrawable(tintable);
            }
        }
    }
}
