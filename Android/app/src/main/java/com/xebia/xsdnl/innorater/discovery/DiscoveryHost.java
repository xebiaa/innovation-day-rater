/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

/**
 * Provider interface for operating a discovery Host.
 */
public interface DiscoveryHost extends Discovery {
    /**
     * Broadcast the given message for nearby listeners.
     * @param sessionName the session name to advertise.
     */
    void advertise(String sessionName, String sessionId);
}
