/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.spectator;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.UUID;

/**
 * Tracks a User ID for casting votes, and keeps it persistent across restarts.
 */
public class UserID {

    private static String PREFS_KEY_USER_ID = "UserID";
    private static String userID;

    public static String getUserID(Context context) {
        if (userID == null) {
            UUID value = loadUserIDOrCreateNew(context);
            userID = value.toString();
        }
        return userID;
    }

    private static UUID loadUserIDOrCreateNew(Context context) {
        UUID result = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String temp = prefs.getString(PREFS_KEY_USER_ID, null);
        if (temp != null) {
            try {
                result = UUID.fromString(temp);
            } catch (IllegalArgumentException ignored) {
                // result is still null
            }
        }
        if (result == null) {
            result = UUID.randomUUID();
            prefs.edit().putString(PREFS_KEY_USER_ID, result.toString()).apply();
        }
        return result;
    }
}
