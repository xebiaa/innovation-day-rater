/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.Strategy;

/**
 * Implements {@link DiscoveryHost} using Google Nearby.
 */
class NearbyDiscoveryHost extends NearbyDiscovery implements DiscoveryHost {

    private static final String TAG = "Nearby";
    public static final Strategy BROADCAST_ONLY = new Strategy.Builder()
            .setDiscoveryMode(Strategy.DISCOVERY_MODE_BROADCAST).build();


    private Message mMessage;

    public NearbyDiscoveryHost(@NonNull Activity context) {
        super(context);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (activity == context) {
            if (mGoogleApiClient.isConnected() && mMessage != null) {
                // Clean up when the user leaves the activity.
                Nearby.Messages.unpublish(mGoogleApiClient, mMessage)
                        .setResultCallback(this);
            }
            mMessage = null;
        }
        super.onActivityStopped(activity);
    }

    @Override
    public void advertise(String sessionName, String sessionId) {
        if (mMessage != null) {
            throw new IllegalStateException("Already broadcasting");
        }
        HostBroadcast.Builder b = new HostBroadcast.Builder();
        b.session_name(sessionName);
        b.session_id(sessionId);
        mMessage = new Message(b.build().toByteArray(), HOST_BROADCAST);
        publishTheMessage();
    }

    @Override
    public void onConnected(Bundle bundle) {
        super.onConnected(bundle);
        publishTheMessage();
    }

    @Override
    protected void onPermissionRequestCompleted(boolean permissionGranted) {
        if (permissionGranted) {
            publishTheMessage();
        }
    }

    private void publishTheMessage() {
        if (mMessage != null) {
            Log.d(TAG, "Publishing the message");
            if (mGoogleApiClient.isConnected()) {
                Nearby.Messages.publish(mGoogleApiClient, mMessage, BROADCAST_ONLY)
                        .setResultCallback(this);
            }
        }
    }
}
