/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.spectator;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.xebia.xsdnl.innorater.R;
import com.xebia.xsdnl.innorater.discovery.DiscoveredSession;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Displays a found session name and id.
 */
public class DiscoveredSessionView extends FrameLayout {

    @Bind(R.id.session_view__session_name)
    TextView tvSessionName;

    private DiscoveredSession session;

    public DiscoveredSessionView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.session_view, this, true);
        ButterKnife.bind(this);
    }

    public void setSession(DiscoveredSession session) {
        this.session = session;
        this.tvSessionName.setText(session.sessionName);
    }

    public DiscoveredSession getSession() {
        return session;
    }
}
