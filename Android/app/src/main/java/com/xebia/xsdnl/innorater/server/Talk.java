/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.server;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Talk {
    private String talkID;
    private String name;
    private List<Vote> votes;

    protected Talk() {
        // Empty constructor for Gson deserialisation.
    }

    public static Talk createNew(String name) {
        String talkID = UUID.randomUUID().toString();
        return new Talk(talkID, name, Collections.<Vote>emptyList());
    }

    private Talk(String talkID, String name, List<Vote> votes) {
        this.talkID = talkID;
        this.name = name;
        this.votes = votes;
    }


    public String getTalkID() {
        return talkID;
    }

    public String getName() {
        return name;
    }

    public List<Vote> getVotes() {
        return votes;
    }
}
