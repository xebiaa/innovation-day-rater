/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import rx.Observable;

/**
 * Provider interface for operating a discovery Spectator.
 */
public interface DiscoverySpectator extends Discovery {

    Observable<DiscoveredSession> discoveryStream();
}
