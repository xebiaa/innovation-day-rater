/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

/**
 * Event object for status updates of the discovery service.
 */
public enum DiscoveryStatus {
    INACTIVE, ACTIVE;
}
