/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Tracks session discovery preference and provides discovery instances.
 */
public class DiscoverySystem {

    private static String PREFS_KEY_DISCO_ENABLED = "DiscoveryEnabled";
    private static Boolean enabled;

    private Activity context;

    private DiscoverySystem(Activity context) {
        this.context = context;
    }

    public static DiscoverySystem getInstance(Activity context) {
        if (enabled == null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            enabled = prefs.getBoolean(PREFS_KEY_DISCO_ENABLED, true);
        }
        return new DiscoverySystem(context);
    }

    public void setEnabled(boolean value) {
        if (enabled != value) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            prefs.edit().putBoolean(PREFS_KEY_DISCO_ENABLED, value).apply();
            enabled = value;
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public DiscoveryHost newHost() {
        return enabled
                ? new NearbyDiscoveryHost(context)
                : new NullDiscovery();
    }

    public DiscoverySpectator newSpectator() {
        return enabled
                ? new NearbyDiscoverySpectator(context)
                : new NullDiscovery();
    }
}
