/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

/**
 * Session data that was found (or lost) through the discovery system.
 */
public class DiscoveredSession {

    public final String sessionName;
    public final String sessionID;
    /**
     * If false, lost (used to be there but no longer is).
     */
    public final boolean found;

    public DiscoveredSession(String sessionID, String sessionName, boolean found) {
        this.sessionName = sessionName;
        this.sessionID = sessionID;
        this.found = found;
    }
}
