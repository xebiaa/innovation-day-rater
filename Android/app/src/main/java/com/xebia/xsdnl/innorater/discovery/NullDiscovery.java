/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.content.Intent;

import rx.Observable;

/**
 * Inert implementation of the discovery mechanism.
 */
class NullDiscovery implements DiscoveryHost, DiscoverySpectator {
    @Override
    public void advertise(String sessionName, String sessionId) {
        // Ignored
    }

    @Override
    public Observable<DiscoveredSession> discoveryStream() {
        return rx.Observable.empty();
    }

    @Override
    public Observable<DiscoveryStatus> statusStream() {
        return rx.Observable.just(DiscoveryStatus.INACTIVE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Ignored
    }
}
