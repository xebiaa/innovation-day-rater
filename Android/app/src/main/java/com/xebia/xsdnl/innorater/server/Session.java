/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.server;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Session {

    private String name;
    private String sessionID;
    private List<Talk> talks;

    protected Session() {
        // Empty constructor for Gson deserialisation.
    }

    public static Session createNew(final String name, final Talk initialTalk) {
        String sessionID = UUID.randomUUID().toString();
        final ArrayList<Talk> talks = new ArrayList<>();
        talks.add(initialTalk);
        return new Session(sessionID, name, talks);
    }

    public Session(final String sessionID, final String name, final List<Talk> talks) {
        this.sessionID = sessionID;
        this.name = name;
        this.talks = talks;
    }

    public String getSessionID() {
        return sessionID;
    }

    public String getName() {
        return name;
    }

    public List<Talk> getTalks() {
        if (talks == null) {
            return Collections.emptyList();
        }
        return talks;
    }

    @Nullable
    public Talk getCurrentTalk() {
        if (talks == null || talks.isEmpty()) {
            return null;
        }
        return talks.get(talks.size() - 1);
    }
}


