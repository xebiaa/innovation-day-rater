/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater;

import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.xebia.xsdnl.innorater.discovery.DiscoverySystem;
import com.xebia.xsdnl.innorater.host.SessionHostActivity;
import com.xebia.xsdnl.innorater.spectator.SessionSelectionActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

/**
 * Lets the operator choose in which mode to run.
 */
public class MainActivity extends AppCompatActivity {

    public static final String STATE_HOST_NAME_REVEALED = "HostNameRevealed";

    @Bind(R.id.main__hostCardContents)
    ViewGroup vgHostCardContents;

    @Bind(R.id.main__hostSessionNameLayout)
    ViewGroup vgHostSessionName;

    @Bind(R.id.main__hostSessionName)
    EditText etHostSessionName;

    @Bind(R.id.main__hostTalkNameLayout)
    ViewGroup vgHostTalkName;

    @Bind(R.id.main__hostTalkName)
    EditText etHostTalkName;

    @Bind(R.id.main__hostStart)
    Button btnHostStart;

    @Bind(R.id.main__nearbySwitch)
    CompoundButton btnDiscoEnabled;

    private boolean etHostSessionNameRevealed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));
        ButterKnife.bind(this);

        final boolean nearbyEnabled = DiscoverySystem.getInstance(this).isEnabled();
        btnDiscoEnabled.setChecked(nearbyEnabled);

        etHostSessionNameRevealed = savedInstanceState != null && savedInstanceState.getBoolean(STATE_HOST_NAME_REVEALED);
        if (etHostSessionNameRevealed) {
            vgHostSessionName.setVisibility(View.VISIBLE);
            vgHostTalkName.setVisibility(View.VISIBLE);
            onHostSessionInfoTextChanged();
        } else {
            btnHostStart.setEnabled(true);
            vgHostSessionName.setVisibility(View.GONE);
            vgHostTalkName.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_HOST_NAME_REVEALED, etHostSessionNameRevealed);
    }

    @OnClick(R.id.main__hostStart)
    @SuppressWarnings("unused")
    void onBtnHostStartClicked() {
        if (etHostSessionNameRevealed) {
            final CharSequence sessionName = Crap.trim(etHostSessionName.getText());
            final CharSequence talkName = Crap.trim(etHostTalkName.getText());
            if (TextUtils.isEmpty(sessionName)) {
                Toast.makeText(this, R.string.main__err_missing_session_name, Toast.LENGTH_SHORT).show();
                return;
            }
            startActivity(SessionHostActivity.createStartIntent(this, sessionName, talkName));
        } else {
            btnHostStart.setEnabled(false);
            vgHostSessionName.setVisibility(View.VISIBLE);
            vgHostTalkName.setVisibility(View.VISIBLE);
            etHostSessionNameRevealed = true;
            etHostTalkName.setVisibility(View.VISIBLE);
            final LayoutTransition transition = vgHostCardContents.getLayoutTransition();
            if (transition != null) {
                transition.addTransitionListener(new LayoutTransition.TransitionListener() {
                    @Override
                    public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                        // Ignored
                    }

                    @Override
                    public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                        etHostSessionName.requestFocus();
                    }
                });
            }
        }
    }

    @OnClick(R.id.main__nearbySwitch)
    @SuppressWarnings("unused")
    void onBtnNearbyDiscoveryClicked() {
        DiscoverySystem.getInstance(this).setEnabled(btnDiscoEnabled.isChecked());
    }

    @OnClick(R.id.main__spectatorStart)
    @SuppressWarnings("unused")
    void onBtnSpectatorStartClicked() {
        startActivity(SessionSelectionActivity.createStartIntent(this));
    }

    @OnTextChanged({R.id.main__hostSessionName, R.id.main__hostTalkName})
    @SuppressWarnings("unused")
    void onHostSessionInfoTextChanged() {
        if (etHostSessionNameRevealed) {
            boolean hasSessionTitle = !TextUtils.isEmpty(Crap.trim(etHostSessionName.getText()));
            boolean hasTalkTitle = !TextUtils.isEmpty(Crap.trim(etHostTalkName.getText()));
            btnHostStart.setEnabled(hasSessionTitle && hasTalkTitle);
        }
    }

    @OnEditorAction(R.id.main__hostTalkName)
    @SuppressWarnings("unused")
    boolean onHostTalkNameEditorAction() {
        final CharSequence sessionName = Crap.trim(etHostSessionName.getText());
        final CharSequence talkName = Crap.trim(etHostTalkName.getText());
        if (TextUtils.isEmpty(sessionName) || TextUtils.isEmpty(talkName)) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etHostSessionName.getWindowToken(), 0);
        } else {
            startActivity(SessionHostActivity.createStartIntent(this, sessionName, talkName));
        }
        return true;
    }
}
