/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.server;

import java.util.UUID;

public class Vote {

    private String voteID;
    private String userID;
    private int voteCategory;
    private int value;

    protected Vote() {
        // Empty constructor for Gson deserialisation.
    }

    public Vote(String userID, int voteCategory, int value) {
        this(userID, UUID.randomUUID().toString(), voteCategory, value);
    }

    public Vote(String userID, String voteID, int voteCategory, int value) {
        this.userID = userID;
        this.voteID = voteID;
        this.voteCategory = voteCategory;
        this.value = value;
    }

    public String getUserID() {
        return userID;
    }

    public String getVoteID() {
        return voteID;
    }

    public int getVoteCategory() {
        return voteCategory;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "categoryId=" + voteCategory +
                ", rating=" + value +
                ", userID='" + userID + '\'' +
                ", voteID='" + voteID + '\'' +
                '}';
    }
}
