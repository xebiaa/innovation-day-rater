/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.spectator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.xebia.xsdnl.innorater.R;
import com.xebia.xsdnl.innorater.server.RaterService;
import com.xebia.xsdnl.innorater.server.Servers;
import com.xebia.xsdnl.innorater.server.Session;
import com.xebia.xsdnl.innorater.server.Talk;
import com.xebia.xsdnl.innorater.server.Vote;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;
import rx.Notification;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;

/**
 * Spectate on a session someone else is hosting.
 */
public class SessionSpectatorActivity extends AppCompatActivity {

    private static final String EXTRA_SESSION_ID = "sessionId";
    private static final String EXTRA_SESSION_NAME = "sessionName";
    private static final String TAG = "SessionSpectatorAct";

    private RaterService mRaterService;
    private Session mSession;
    private Talk mTalk;
    private String mSessionName;
    private String mUserID;

    @Bind(R.id.spectator__talk_title)
    TextView tvSessionTitle;

    @Bind({R.id.spectator__rating_bar_1, R.id.spectator__rating_bar_2, R.id.spectator__rating_bar_3, R.id.spectator__rating_bar_4})
    RatingBar[] ratingBars;
    RatingBarChangeListener[] ratingBarListeners;

    public static Intent createStartIntent(@NonNull Context context, @NonNull String sessionName) {
        return createStartIntent(context, sessionName, null);
    }

    public static Intent createStartIntent(@NonNull Context context, @NonNull String sessionName,
                                           @Nullable String sessionId) {
        Intent i = new Intent(context, SessionSpectatorActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(EXTRA_SESSION_ID, sessionId);
        i.putExtra(EXTRA_SESSION_NAME, sessionName);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_spectator);
        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));
        ButterKnife.bind(this);
        mUserID = UserID.getUserID(this);
        String sessionId = getIntent().getStringExtra(EXTRA_SESSION_ID);
        mSessionName = getIntent().getStringExtra(EXTRA_SESSION_NAME);
        setTitle(getString(R.string.spectator__window_title_2, mSessionName));

        mRaterService = Servers.raterService();
        (sessionId != null
                ? mRaterService.getSessionById(sessionId)
                : mRaterService.getSessionByName(mSessionName))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSessionObserver);

        ratingBarListeners = new RatingBarChangeListener[ratingBars.length];
        for (int i = 0; i < ratingBars.length; i++) {
            final RatingBar rb = ratingBars[i];
            rb.setEnabled(false);
            ratingBarListeners[i] = new RatingBarChangeListener(rb, i + 1);
        }
    }

    @Override
    protected void onDestroy() {
        for (RatingBarChangeListener rbl : ratingBarListeners) {
            rbl.destroy();
        }
        super.onDestroy();
    }

    private final Observer<Session> getSessionObserver = new Observer<Session>() {
        @Override
        public void onNext(Session session) {
            Log.i(TAG, "success obtain session (num talks = " + session.getTalks().size() + ')');
            mSession = session;
            if (session.getTalks().isEmpty()) {
                final Toast toast = Toast.makeText(SessionSpectatorActivity.this, getString(R.string.spectator__session_no_talks, mSessionName), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                finish();
                return;
            }
            int numTalks = session.getTalks().size();
            mTalk = session.getTalks().get(numTalks - 1);
            tvSessionTitle.setText(mTalk.getName());
            for (RatingBar rb : ratingBars) {
                rb.setEnabled(true);
            }
        }

        @Override
        public void onCompleted() {
            // Ignored
        }

        @Override
        public void onError(Throwable error) {
            Log.i(TAG, "error obtain session " + error.getMessage());
            final Toast toast = Toast.makeText(SessionSpectatorActivity.this, getString(R.string.spectator__session_not_found, mSessionName), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            finish();
        }
    };


    private class RatingBarChangeListener implements RatingBar.OnRatingBarChangeListener {
        private final RatingBar ratingBar;
        private final int categoryID;
        private final PublishSubject<Vote> mRatingClickStream;

        public RatingBarChangeListener(@NonNull RatingBar ratingBar, int categoryID) {
            this.ratingBar = ratingBar;
            this.categoryID = categoryID;
            this.mRatingClickStream = PublishSubject.create();
            this.mRatingClickStream
                    .doOnEach(this.disableRatingBar)
                    .flatMap(this.submitVoteToNetwork)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this.enableRatingBar);
            this.ratingBar.setOnRatingBarChangeListener(this);
        }

        void destroy() {
            this.ratingBar.setOnRatingBarChangeListener(null);
            this.mRatingClickStream.onCompleted();
        }

        @Override
        public void onRatingChanged(final RatingBar ratingBar, float rating, boolean fromUser) {
            int value = (int) rating;
            if (fromUser && value > 0 && value < 6) {
                // Send a Vote object on its way to the network
                Vote vote = new Vote(mUserID, categoryID, value);
                this.mRatingClickStream.onNext(vote);
            }
        }

        private final Action1<Notification<? super Vote>> disableRatingBar = new Action1<Notification<? super Vote>>() {
            @Override
            public void call(Notification<? super Vote> notification) {
                ratingBar.setEnabled(false);
            }
        };

        private Func1<Vote, Observable<Void>> submitVoteToNetwork = new Func1<Vote, Observable<Void>>() {
            @Override
            public Observable<Void> call(Vote vote) {
                return mRaterService.vote(mSession.getSessionID(), mTalk.getTalkID(), vote);
            }
        };

        private final Observer<Void> enableRatingBar = new Observer<Void>() {
            @Override
            public void onNext(Void ignored) {
                ratingBar.setEnabled(true);
                Log.i("Spectate", "Submit vote successful.");
            }

            @Override
            public void onError(Throwable t) {
                ratingBar.setEnabled(true);
                final Toast toast = Toast.makeText(SessionSpectatorActivity.this, R.string.spectator__vote_error,
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                if (t instanceof RetrofitError) {
                    RetrofitError re = (RetrofitError) t;
                    Log.i("Spectate", "Submit vote error: " + re.getMessage());
                }
            }

            @Override
            public void onCompleted() {
                //ignored
            }
        };
    }
}
