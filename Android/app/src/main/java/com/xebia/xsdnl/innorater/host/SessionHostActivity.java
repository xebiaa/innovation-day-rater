/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.host;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.xebia.xsdnl.innorater.Crap;
import com.xebia.xsdnl.innorater.R;
import com.xebia.xsdnl.innorater.discovery.DiscoveryHost;
import com.xebia.xsdnl.innorater.discovery.DiscoveryStatus;
import com.xebia.xsdnl.innorater.discovery.DiscoverySystem;
import com.xebia.xsdnl.innorater.server.RaterService;
import com.xebia.xsdnl.innorater.server.Servers;
import com.xebia.xsdnl.innorater.server.Session;
import com.xebia.xsdnl.innorater.server.Talk;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Host a session for others to join and vote.
 */
public class SessionHostActivity extends AppCompatActivity {

    private static final String TAG = "SessionHostActivity";
    private static final String EXTRA_SESSION_NAME = "SessionName";
    private static final String STATE_SESSION_ID = "SessionID";
    private static final String EXTRA_TALK_NAME = "TalkName";

    private DiscoveryHost discovery;
    private RaterService mRaterService;

    private Session mSession;

    @Bind(R.id.host__talk_title)
    TextView tvTalkTitle;

    @Bind(R.id.host__broadcasting_session_text)
    TextView tvNearbyText;

    @Bind(R.id.host__nearbyLogo)
    ImageView ivNearbyLogo;

    @Bind({R.id.host__rating_bar_1, R.id.host__rating_bar_2, R.id.host__rating_bar_3, R.id.host__rating_bar_4})
    RatingBar[] ratingBars;

    public static Intent createStartIntent(@NonNull Context context, @NonNull CharSequence sessionName, @NonNull CharSequence talkName) {
        Intent i = new Intent(context, SessionHostActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(EXTRA_SESSION_NAME, sessionName.toString());
        i.putExtra(EXTRA_TALK_NAME, talkName.toString());
        return i;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String mSessionName = getIntent().getStringExtra(EXTRA_SESSION_NAME);
        String mTalkName = getIntent().getStringExtra(EXTRA_TALK_NAME);
        String restoredSessionId = savedInstanceState != null ? savedInstanceState.getString(STATE_SESSION_ID, null) : null;
        if (mSessionName == null || TextUtils.isEmpty(mSessionName.trim())) {
            Log.w(TAG, "Missing or empty extra Intent." + EXTRA_SESSION_NAME + " (String), aborting.");
            finish();
        }
        if (mTalkName == null || TextUtils.isEmpty(mTalkName.trim())) {
            Log.w(TAG, "Missing or empty extra Intent." + EXTRA_TALK_NAME + " (String), aborting.");
            finish();
        }
        setTitle(getString(R.string.host__window_title_2, mSessionName));
        setContentView(R.layout.activity_session_host);
        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));
        ButterKnife.bind(this);

        discovery = DiscoverySystem.getInstance(this).newHost();
        discovery.statusStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(discoveryStatusObserver);

        final Session newSession = Session.createNew(mSessionName, Talk.createNew(mTalkName));
        mRaterService = Servers.raterService();
        (restoredSessionId == null
                ? mRaterService.createSession(newSession)
                : mRaterService.getSessionById(restoredSessionId))
                .doOnNext(publishSessionNearby)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(updateUI);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSession != null) {
            outState.putString(STATE_SESSION_ID, mSession.getSessionID());
        }
    }

    void updateRatingAverages() {
        final Talk talk = mSession.getCurrentTalk();
        if (talk != null) {
            mRaterService.getTalkById(mSession.getSessionID(), talk.getTalkID())
                .map(new Func1<Talk, VoteAverages>() {
                    @Override
                    public VoteAverages call(Talk talk) {
                        return VoteAverages.of(talk);
                    }
                })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<VoteAverages>() {
                @Override
                public void call(VoteAverages averages) {
                    for (int i = 0; i < 4; i++) {
                        ratingBars[i].setRating(averages.score[i]);
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        discovery.onActivityResult(requestCode, resultCode, data);
    }

    private Action1<? super Session> publishSessionNearby = new Action1<Session>() {
        @Override
        public void call(Session session) {
            discovery.advertise(session.getName(), session.getSessionID());
        }
    };

    private Observer<Session> updateUI = new Observer<Session>() {
        @Override
        public void onCompleted() {
            // Ignored
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error create session on server " + e.getMessage());
            Toast t = Toast.makeText(SessionHostActivity.this, R.string.host__create_session_failed, Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            finish();
        }

        @Override
        public void onNext(Session session) {
            Log.d(TAG, "Success create session on server: " + session.getName() + ", " + session.getSessionID() + ", talks=" + session.getTalks().size());
            SessionHostActivity.this.mSession = session;
            final Talk currentTalk = session.getCurrentTalk();
            if (currentTalk != null) {
                tvTalkTitle.setText(currentTalk.getName());
                updateRatingAverages();
            } else {
                // TODO once the "add talk to current session" thing is there, use that.
                final Toast toast = Toast.makeText(SessionHostActivity.this, getString(R.string.spectator__session_no_talks, session.getName()), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                finish();
            }
        }
    };

    private final Observer<DiscoveryStatus> discoveryStatusObserver = new Observer<DiscoveryStatus>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(DiscoveryStatus discoveryStatus) {
            boolean active = discoveryStatus == DiscoveryStatus.ACTIVE;
            tvNearbyText.setText(active ? R.string.host__broadcasting_nearby_enabled : R.string.host__broadcasting_nearby_disabled);
            Crap.showNearbyStateInImageView(ivNearbyLogo, active);
        }
    };

    // TODO set up & show timer

    // TODO when timer expires, make all participants sound an alarm

    /**
     * We don't really want a refresh button, but it works as a 1.0 prototype.
     */
    @OnClick(R.id.host__temp_refresh)
    @SuppressWarnings("unused")
    public void refreshButtonClick() {
        updateRatingAverages();
    }

}
