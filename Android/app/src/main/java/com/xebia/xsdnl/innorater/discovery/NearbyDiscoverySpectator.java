/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Strategy;
import com.squareup.wire.Wire;

import java.io.IOException;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Implements {@link DiscoverySpectator} using Google Nearby.
 */
class NearbyDiscoverySpectator extends NearbyDiscovery implements DiscoverySpectator {

    private static final String TAG = "Nearby";
    public static final Strategy SCAN_ONLY = new Strategy.Builder()
            .setDiscoveryMode(Strategy.DISCOVERY_MODE_SCAN).build();

    private final MessageListener mMessageListener = new MessageListener() {
        @Override
        public void onFound(Message message) {
            NearbyDiscoverySpectator.this.onMessage(message, true);
        }

        @Override
        public void onLost(Message message) {
            NearbyDiscoverySpectator.this.onMessage(message, false);
        }
    };

    private final PublishSubject<DiscoveredSession> sessionPublisher;

    public NearbyDiscoverySpectator(@NonNull Activity context) {
        super(context);
        sessionPublisher = PublishSubject.create();
    }

    @Override
    public Observable<DiscoveredSession> discoveryStream() {
        return sessionPublisher;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (activity == context) {
            sessionPublisher.onCompleted();
            if (mGoogleApiClient.isConnected()) {
                // Clean up when the user leaves the activity.
                Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                        .setResultCallback(this);
            }
        }
        super.onActivityStopped(activity);
    }

    @Override
    public void onConnected(Bundle bundle) {
        super.onConnected(bundle);
        subscribeToMessages();
    }

    @Override
    protected void onPermissionRequestCompleted(boolean permissionGranted) {
        if (permissionGranted) {
            subscribeToMessages();
        } else {
            sessionPublisher.onCompleted();
        }
    }

    private void subscribeToMessages() {
        Log.d(TAG, "Listening for messages");
        Nearby.Messages.subscribe(mGoogleApiClient, this.mMessageListener, SCAN_ONLY)
                .setResultCallback(this);
    }

    void onMessage(Message message, boolean found) {
        Log.d(TAG, "onMessage, event= " + (found ? "found" : "lost") + " type=" + message.getType());
        if (HOST_BROADCAST.equals(message.getType())) {
            Wire wire = new Wire();
            try {
                final HostBroadcast broadcast = wire.parseFrom(message.getContent(), HostBroadcast.class);
                Log.i(TAG, (found ? "Found" : "Lost") + " HostBroadcast, name=" + broadcast.session_name);
                final DiscoveredSession session = new DiscoveredSession(broadcast.session_id, broadcast.session_name, found);
                sessionPublisher.onNext(session);
            } catch (IOException e) {
                Log.w(TAG, "Deserialization failure", e);
            }
        }
    }
}
