/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.xebia.xsdnl.innorater.R;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Base implementation of {@link Discovery} using Google Nearby.
 */
abstract class NearbyDiscovery implements Discovery,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status>,
        Application.ActivityLifecycleCallbacks {

    protected static final String HOST_BROADCAST = "HB";
    private static final String TAG = "Nearby";
    private static final int REQUEST_CODE_STATUS_RESOLUTION = 996;
    protected Activity context;
    protected GoogleApiClient mGoogleApiClient;
    private PublishSubject<DiscoveryStatus> statusPublisher;

    public NearbyDiscovery(@NonNull Activity context) {
        this.context = context;
        statusPublisher = PublishSubject.create();
        context.getApplication().registerActivityLifecycleCallbacks(this);
    }

    @Override
    public Observable<DiscoveryStatus> statusStream() {
        return statusPublisher;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        // Ignored
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (activity == context) {
            mGoogleApiClient = new GoogleApiClient.Builder(this.context)
                    .addApi(Nearby.MESSAGES_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            statusPublisher.onNext(DiscoveryStatus.INACTIVE);
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        // Ignored
    }

    @Override
    public void onActivityPaused(Activity activity) {
        // Ignored
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (activity == context) {
            if (mGoogleApiClient.isConnected()) {
                statusPublisher.onNext(DiscoveryStatus.INACTIVE);
                mGoogleApiClient.disconnect();
            }
            Log.d(TAG, "disconnected");
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        // Ignored
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity == context) {
            context.getApplication().unregisterActivityLifecycleCallbacks(this);
            statusPublisher.onCompleted();
            mGoogleApiClient = null;
            context = null;
            Log.d(TAG, "destroyed");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected: " + bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: " + i);
        statusPublisher.onNext(DiscoveryStatus.INACTIVE);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult);
        statusPublisher.onNext(DiscoveryStatus.INACTIVE);
    }

    @Override
    public void onResult(Status status) {
        Log.d(TAG, "onResult: " + status + (status.hasResolution() ? " resolvable" : ""));
        if (!status.isSuccess()) {
            if (status.hasResolution()) {
                try {
                    status.startResolutionForResult(context, REQUEST_CODE_STATUS_RESOLUTION);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "onResult exception", e);
                }
            } else {
                Toast.makeText(context, R.string.nearby_permission_failure, Toast.LENGTH_SHORT).show();
            }
        } else {
            statusPublisher.onNext(DiscoveryStatus.ACTIVE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_STATUS_RESOLUTION) {
            final boolean ok = resultCode == Activity.RESULT_OK;
            onPermissionRequestCompleted(ok);
            statusPublisher.onNext(ok ? DiscoveryStatus.ACTIVE : DiscoveryStatus.INACTIVE);
        }
    }

    protected abstract void onPermissionRequestCompleted(boolean permissionGranted);
}
