/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.discovery;

import android.content.Intent;

import rx.Observable;

/**
 * Provider interface for Host/Spectator discovery.
 */
public interface Discovery {

    Observable<DiscoveryStatus> statusStream();

    /**
     * Client activity must forward its {@code onActivityResult(…)} events into the discovery system,
     * this is to make the permission prompts work.
     */
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
