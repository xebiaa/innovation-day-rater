/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.host;

import com.xebia.xsdnl.innorater.server.Talk;
import com.xebia.xsdnl.innorater.server.Vote;

import java.util.List;

/**
 * Calculates and holds the averages for a bunch of votes.
 */
class VoteAverages {

    public static VoteAverages of(Talk talk) {
        return new VoteAverages(talk.getVotes());
    }

    public final float[] score;

    private VoteAverages(List<Vote> votes) {
        super();
        score = new float[4];
        float[] sums = new float[4];
        float[] counts = new float[4];
        if (votes != null) {
            for (int i = 0, max = votes.size(); i < max; i++) {
                Vote vote = votes.get(i);
                int catIdx = vote.getVoteCategory() - 1;
                int value = vote.getValue();
                if (catIdx >= 0 && catIdx < 4 && value >= 1 && value < 6) {
                    sums[catIdx] += value;
                    counts[catIdx] += 1f;
                }
            }
            for (int i = 0; i < 4; i++) {
                final float avg = sums[i] / counts[i];
                if (Float.isNaN(avg)) {
                    score[i] = 0f;
                } else {
                    score[i] = avg;
                }
            }
        }
    }

}
