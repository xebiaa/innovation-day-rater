/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.spectator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xebia.xsdnl.innorater.Crap;
import com.xebia.xsdnl.innorater.R;
import com.xebia.xsdnl.innorater.discovery.DiscoveredSession;
import com.xebia.xsdnl.innorater.discovery.DiscoverySpectator;
import com.xebia.xsdnl.innorater.discovery.DiscoveryStatus;
import com.xebia.xsdnl.innorater.discovery.DiscoverySystem;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Select a session someone else is hosting, so that you may spectate on it.
 */
public class SessionSelectionActivity extends AppCompatActivity
        implements View.OnClickListener {

    private final ArrayMap<String, Integer> foundSessionViews = new ArrayMap<>(8);

    private DiscoverySpectator discovery;

    @Bind(R.id.selector__manual_session_name)
    EditText etManualSessionName;

    @Bind(R.id.selector__manual_session_start)
    Button btnManualSessionJoin;

    @Bind(R.id.selector__discovered_session_parent)
    LinearLayout llDiscoveredSessions;

    @Bind(R.id.selector__discovery_active)
    View vDiscoveryActive;

    @Bind(R.id.selector__discovery_disabled)
    View vDiscoveryDisabled;

    @Bind(R.id.selector__nearbyLogo)
    ImageView ivNearbyLogo;

    public static Intent createStartIntent(@NonNull Context context) {
        final Intent i = new Intent(context, SessionSelectionActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_selection);
        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));
        ButterKnife.bind(this);
        discovery = DiscoverySystem.getInstance(this).newSpectator();
        discovery.discoveryStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(discoveredSessionObserver);
        discovery.statusStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(discoveryStatusObserver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        discovery.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DiscoveredSessionView) {
            DiscoveredSessionView sv = (DiscoveredSessionView) v;
            DiscoveredSession session = sv.getSession();
            startActivity(SessionSpectatorActivity.createStartIntent(this, session.sessionName, session.sessionID));
        }
    }

    private void addSessionView(DiscoveredSession session) {
        int id = View.generateViewId();
        DiscoveredSessionView sessionView = new DiscoveredSessionView(this);
        sessionView.setId(id);
        sessionView.setSession(session);
        sessionView.setClickable(true);
        sessionView.setOnClickListener(this);
        llDiscoveredSessions.addView(sessionView);
        foundSessionViews.put(session.sessionID, id);
    }

    private void removeSessionView(DiscoveredSession session) {
        Integer id = foundSessionViews.get(session.sessionID);
        if (id != null) {
            View v = llDiscoveredSessions.findViewById(id);
            llDiscoveredSessions.removeView(v);
            foundSessionViews.remove(session.sessionID);
        }
    }

    private final Observer<DiscoveredSession> discoveredSessionObserver = new Observer<DiscoveredSession>() {
        public void onNext(DiscoveredSession ds) {
            if (ds.found) {
                addSessionView(ds);
            } else {
                removeSessionView(ds);
            }
        }

        public void onError(Throwable err) {
            // Ignored.
        }

        public void onCompleted() {
            // Ignored
        }
    };

    @OnTextChanged(R.id.selector__manual_session_name)
    @SuppressWarnings("unused")
    void onManualSessionNameTextChanged(CharSequence text) {
        btnManualSessionJoin.setEnabled(!TextUtils.isEmpty(Crap.trim(text)));
    }

    @OnClick(R.id.selector__manual_session_start)
    @SuppressWarnings("unused")
    void onBtnManualSessionJoinClicked() {
        String sessionName = Crap.trim(etManualSessionName.getText()).toString();
        startActivity(SessionSpectatorActivity.createStartIntent(this, sessionName));
    }

    @OnEditorAction(R.id.selector__manual_session_name)
    @SuppressWarnings("unused")
    boolean onHostSessionNameEditorAction() {
        final CharSequence sessionName = Crap.trim(etManualSessionName.getText());
        if (TextUtils.isEmpty(sessionName)) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etManualSessionName.getWindowToken(), 0);
        } else {
            startActivity(SessionSpectatorActivity.createStartIntent(this, sessionName.toString()));
        }
        return true;
    }

    private final Observer<DiscoveryStatus> discoveryStatusObserver = new Observer<DiscoveryStatus>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(DiscoveryStatus discoveryStatus) {
            boolean active = discoveryStatus == DiscoveryStatus.ACTIVE;
            vDiscoveryDisabled.setVisibility(active ? View.GONE : View.VISIBLE);
            vDiscoveryActive.setVisibility(!active ? View.GONE : View.VISIBLE);
            Crap.showNearbyStateInImageView(ivNearbyLogo, active);
        }
    };

}
