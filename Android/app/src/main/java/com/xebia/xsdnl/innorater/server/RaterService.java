/*
 * Copyright 2015 Xebia Nederland B.V.
 *
 * License:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * http://creativecommons.org/licenses/by-nc/4.0/
 */

package com.xebia.xsdnl.innorater.server;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface RaterService {
    @POST("/session")
    Observable<Session> createSession(@Body Session session);

    @GET("/session")
    Observable<Session> getSessionByName(@Query("name") String name);

    @GET("/session/{sessionId}")
    Observable<Session> getSessionById(@Path("sessionId") String sessionId);

    @GET("/session/{sessionId}/talk/{talkId}")
    Observable<Talk> getTalkById(@Path("sessionId") String sessionId, @Path("talkId") String talkId);

    @POST("/session/{sessionId}/talk")
    Observable<Talk> createTalk(@Path("sessionId") String sessionId, @Body Talk talk);

    @POST("/session/{sessionId}/talk/{talkId}/vote")
    Observable<Void> vote(@Path("sessionId") String sessionId, @Path("talkId") String talkId, @Body Vote vote);

}
