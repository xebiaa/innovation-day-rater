# Building the project

## Preparation

1. Have JDK 8 installed and set `JAVA_HOME` environment var.
2. Have the Android SDK installed and set `ANDROID_HOME` environment var.
3. Copy `app/secrets.gradle.example` to `app/secrets.gradle` and fill in
   the blanks.

## Compilation

1. `./gradlew assembleDebug`

# Editing the project

Standard Android Studio stuff. Just open this directory in the IDE.
