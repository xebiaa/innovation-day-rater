//
//  ViewController.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController, RatingViewDelegate {

    var model: Model!

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var originalityRatingView: RatingView!
    @IBOutlet weak var creativityRatingView: RatingView!
    @IBOutlet weak var realisticRatingView: RatingView!
    @IBOutlet weak var technologyRatingView: RatingView!

    @IBOutlet weak var timerLabel: UILabel!
    
    var hostMode = false {
        didSet {
            if let toolbar = toolbar {
                toolbar.hidden = !hostMode
            }
        }
    }

    var votesPoller: NSTimer?

    let timeBetweenTalks: NSTimeInterval = 5 * 60
    var nextTalkDate: NSDate?

    var updateTimerTimer: NSTimer?

    var talkUpdaterTimer: NSTimer?

    override func viewDidLoad() {
        super.viewDidLoad()

        if hostMode {
            nextTalk()

            votesPoller = NSTimer(timeInterval: 10, target: self, selector: "updateVotes", userInfo: nil, repeats: true)
            NSRunLoop.currentRunLoop().addTimer(votesPoller!, forMode: NSDefaultRunLoopMode)
        } else {
            talkUpdaterTimer = NSTimer(timeInterval: 60, target: self, selector: "updateTalk", userInfo: nil, repeats: true)
            NSRunLoop.currentRunLoop().addTimer(talkUpdaterTimer!, forMode: NSDefaultRunLoopMode)
            talkUpdaterTimer!.fire()
            updateTimerTimer = NSTimer(timeInterval: 1, target: self, selector: "updateTimer", userInfo: nil, repeats: true)
            NSRunLoop.currentRunLoop().addTimer(updateTimerTimer!, forMode: NSDefaultRunLoopMode)
            timerLabel.hidden = false
        }

        toolbar.hidden = !hostMode

        originalityRatingView.delegate = self
        creativityRatingView.delegate = self
        realisticRatingView.delegate = self
        technologyRatingView.delegate = self

        updateBackgroundColor()
    }

    func updateTimer() {
        if let nextTalkDate = nextTalkDate {
            let secondsToNext = nextTalkDate.timeIntervalSinceNow

            let minutes = Int(secondsToNext / 60)
            let seconds = Int(secondsToNext - Double(minutes * 60))

            timerLabel.text = String(format: "%02d:%02d", minutes, seconds)

            if secondsToNext <= 0 && hostMode {
                nextTalk()
            }
        } else {
            timerLabel.text = ""
        }
    }

    func updateVotes() {
        model.votes()?
            .then { [weak self] votes -> Void in


                var originalityCount = 0
                var creativityCount = 0
                var realisticCount = 0
                var technologyCount = 0

                var averages: [VoteCategory: Int] = [VoteCategory.Originality : 0, VoteCategory.Creativity : 0, VoteCategory.Realistic : 0, VoteCategory.Technology : 0]

                for vote in votes {
                    if let value = vote.value {
                        switch vote.category {
                        case .Originality:
                            originalityCount++
                        case .Creativity:
                            creativityCount++
                        case .Realistic:
                            realisticCount++
                        case .Technology:
                            technologyCount++
                        }
                    }
                }

                if let total = averages[.Originality] where originalityCount > 0 {
                    if let rating = Rating(rawValue: total/originalityCount) {
                        self?.originalityRatingView.rating = rating
                    }
                }
                if let total = averages[.Creativity] where creativityCount > 0 {
                    if let rating = Rating(rawValue: total/creativityCount) {
                        self?.creativityRatingView.rating = rating
                    }
                }

                if let total = averages[.Realistic] where realisticCount > 0 {
                    if let rating = Rating(rawValue: total/realisticCount) {
                        self?.realisticRatingView.rating = rating
                    }
                }

                if let total = averages[.Technology] where technologyCount > 0 {
                    if let rating = Rating(rawValue: total/technologyCount) {
                        self?.technologyRatingView.rating = rating
                    }
                }

                self?.updateBackgroundColor()
        }
    }

    func updateBackgroundColor() {

        var totalRating = 0
        switch(self.originalityRatingView.rating) {
        case .None:
            totalRating += 3
        default:
            totalRating += self.originalityRatingView.rating.rawValue
        }
        switch(self.creativityRatingView.rating) {
        case .None:
            totalRating += 3
        default:
            totalRating += self.creativityRatingView.rating.rawValue
        }
        switch(self.realisticRatingView.rating) {
        case .None:
            totalRating += 3
        default:
            totalRating += self.realisticRatingView.rating.rawValue
        }
        switch(self.technologyRatingView.rating) {
        case .None:
            totalRating += 3
        default:
            totalRating += self.technologyRatingView.rating.rawValue
        }

        let percentage = CGFloat(totalRating - 4) / 16.0
        println("\(percentage) \(totalRating)")
        var newColor = UIColor(red: 1.0 - percentage, green: percentage, blue: 0.0, alpha: 1.0)

        self.view.backgroundColor = newColor
    }

    func updateTalk() {
        model.lastTalk()?
            .then { talk -> Void in
                self.originalityRatingView.rating = .None
                self.creativityRatingView.rating = .None
                self.realisticRatingView.rating = .None
                self.technologyRatingView.rating = .None
        }
    }

    func nextTalk() {
        model.createTalk()?.then {Talk -> Void in
            self.nextTalkDate = NSDate(timeIntervalSinceNow: self.timeBetweenTalks)
        }
    }

    func didRate(rating: Rating, ratingView: RatingView) {
        let category: VoteCategory
        if originalityRatingView == ratingView {
            category = .Originality
        } else if creativityRatingView == ratingView {
            category = .Creativity
        } else if realisticRatingView == ratingView {
            category = .Realistic
        } else if technologyRatingView == ratingView {
            category = .Technology
        } else {
            fatalError("This should not happen")
        }

        model.vote(category, value: rating.rawValue)
        updateBackgroundColor()
    }

    func shouldAllowVoting() -> Bool {
        return !hostMode
    }

}
