//
//  ServerApi.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import PromiseKit
import OMGHTTPURLRQ

class ServerApi: Model {

    private static let url = "http://xebia-innovation-day-rater.appspot.com"

    private static let sessionPath = "session"
    private static let talkPath = "talk"
    private static let votePath = "vote"

    override func createSession(name: String) -> Promise<Session> {
        return super.createSession(name)
            .then { session in
                return session.POST()
        }
    }

    override func createTalk() -> Promise<Talk>? {
        return super.createTalk()?
            .then { talk in
                return talk.POST()
        }
    }

    override func votes() -> Promise<[Vote]>? {
        return session?.talks.last?.GETVotes()
    }

    override func joinSession(name: String) -> Promise<Session> {
        return Session.GET(name)
    }

    override func vote(category: VoteCategory, value: Int) -> Promise<Vote>? {
        if let talk: Talk = session?.talks.last {
            if let vote = talk.votes.filter({$0.category == category}).first {
                let method = vote.value == nil ? Vote.POST : Vote.PUT
                vote.value = value
                return method(vote)()
            }
        }
        return nil
    }

    override func lastTalk() -> Promise<Talk>? {
        return session?.GETLastTalk()
    }

}

extension Session {

    func POST() -> Promise<Session> {
        let p: Promise<NSDictionary> = NSURLConnection.POST("\(ServerApi.url)/\(ServerApi.sessionPath)", JSON: [
            "Name": name,
            "SessionID": uuid
            ])
        return p.then { json -> Session in
                return self
        }
    }

    class func GET(name: String) -> Promise<Session> {
        return NSURLConnection.GET("\(ServerApi.url)/\(ServerApi.sessionPath)", query: ["name": name])
            .then { json in
                return Session(json: json)
        }
    }

    func GETLastTalk() -> Promise<Talk> {
        return NSURLConnection.GET("\(ServerApi.url)/\(ServerApi.sessionPath)/\(uuid)/\(ServerApi.talkPath)")
            .then { json in
                return Talk(json: json, session: self)
        }
    }

    convenience init(json: NSDictionary) {
        self.init(name: json["Name"] as! String, uuid: json["SessionID"] as! String)
    }
}

extension Talk {

    func POST() -> Promise<Talk> {
        let p: Promise<NSDictionary> =  NSURLConnection.POST("\(ServerApi.url)/\(ServerApi.sessionPath)/\(session.uuid)/\(ServerApi.talkPath)", JSON: [
            "talkId": uuid
            ])
        return p.then { json -> Talk in
                return self
        }
    }

    func GETVotes() -> Promise<[Vote]> {
        let p: Promise<NSArray> = NSURLConnection.GET("\(ServerApi.url)/\(ServerApi.sessionPath)/\(session.uuid)/\(ServerApi.talkPath)/\(uuid)/\(ServerApi.votePath)")
        return p.then { jsonArray -> [Vote] in
                var newVotes = [Vote]()
                for jsonDictionary in jsonArray {
                    let vote = Vote(json: jsonDictionary as! NSDictionary, talk: self)
                    newVotes.append(vote)
                }
                self.votes = newVotes
                return newVotes
        }
    }

    convenience init(json: NSDictionary, session: Session) {
        self.init(uuid: json["TalkId"] as! String, session: session)
    }
}

extension Vote {

    func POST() -> Promise<Vote> {
        let p: Promise<NSDictionary> = NSURLConnection.POST("\(ServerApi.url)/\(ServerApi.sessionPath)/\(talk.session.uuid)/\(ServerApi.talkPath)/\(talk.uuid)\(ServerApi.votePath)", JSON: [
            "VoteCategory": category.rawValue,
            "Value": value ?? 0,
            "VoteId": uuid
            ])
        return p.then { json -> Vote in
                return self
        }
    }

    func PUT() -> Promise<Vote> {
        let p: Promise<NSDictionary> = NSURLConnection.promise(OMGHTTPURLRQ.PUT("\(ServerApi.url)/\(ServerApi.sessionPath)/\(talk.session.uuid)/\(ServerApi.talkPath)/\(talk.uuid)\(ServerApi.votePath)/\(uuid)", JSON: [
            "VoteCategory": category.rawValue,
            "Value": value ?? 0,
            "VoteId": uuid
            ]))
        return p.then { json -> Vote in
                return self
        }
    }

    convenience init(json: NSDictionary, talk: Talk) {
        self.init(category: VoteCategory(rawValue: json["VoteCategory"] as! Int)!, value: json["Value"] as? Int, uuid: json["VoteId"] as! String, talk: talk)
    }
    
}