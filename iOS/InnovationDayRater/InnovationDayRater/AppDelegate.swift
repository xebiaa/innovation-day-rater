//
//  AppDelegate.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let messageManager = GNSMessageManager(APIKey: "AIzaSyB4R-x0F4HgQoNlvDsy6d1Dj790UrxOl8g")

    // The publication is active as long as the publication object exists. To stop publishing, release the publication object.
    let message = GNSMessage(content: "hoj: \(NSDate().timeIntervalSince1970)".dataUsingEncoding(NSUTF8StringEncoding))

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.

        println("asdf")
        messageManager.publicationWithMessage(message)
        messageManager.subscriptionWithMessageFoundHandler({ message in
            let str = NSString(data: message.content, encoding: NSUTF8StringEncoding)
            println("Received message: \(str)")
        }, messageLostHandler: { message in
            println("Message lost: \(message.description)")
        })

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

