//
//  Session.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class Session: NSObject {

    let name: String
    let uuid: String

    var talks = [Talk]()

    init(name: String, uuid: String) {
        self.name = name
        self.uuid = uuid
    }

    convenience init(name: String) {
        self.init(name: name, uuid: NSUUID().UUIDString)
    }

    func createTalk() -> Talk {
        let talk = Talk(session: self)
        talks.append(talk)
        return talk
    }
}
