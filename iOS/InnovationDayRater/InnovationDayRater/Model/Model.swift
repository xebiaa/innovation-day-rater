//
//  Model.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import PromiseKit

class Model: NSObject {

    var session: Session?

    // MARK: - Host methods

    func createSession(name: String) -> Promise<Session> {
        return Promise { fulfill, _ in
            fulfill(Session(name: name))
        }
            .then { session -> Session in
                self.session = session
                return session
        }
    }

    func createTalk() -> Promise<Talk>? {
        if let session = session {
            return Promise { fulfill, _ in
                fulfill(session.createTalk())
            }
        } else {
            return nil
        }
    }

    func votes() -> Promise<[Vote]>? {
        if let talk = session?.talks.last {
            return Promise { fulfill, _ in
                fulfill(talk.votes)
            }
        } else {
            return nil
        }
    }

    // MARK: - Client methods

    func joinSession(name: String) -> Promise<Session> {
        return Promise { fulfill, reject in
            let session = Session(name: name) // Dummy
            self.session = session
            fulfill(session)
        }
    }

    func vote(category: VoteCategory, value: Int) -> Promise<Vote>? {
        if let talk: Talk = session?.talks.last {
            if let vote = talk.votes.filter({$0.category == category}).first {
                vote.value = value
                return Promise { fulfill, _ in fulfill(vote) }
            }
        }
        return nil
    }

    func lastTalk() -> Promise<Talk>? {
        if let talk = session?.talks.last {
            return Promise { fulfill, _ in
                fulfill(talk)
            }
        } else {
            return nil
        }
    }

   
}
