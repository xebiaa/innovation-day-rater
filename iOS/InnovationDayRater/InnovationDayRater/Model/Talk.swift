//
//  Talk.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class Talk: NSObject {

    let uuid: String
    unowned let session: Session

    var votes = [Vote]()

    init(uuid: String, session: Session) {
        self.uuid = uuid
        self.session = session

        super.init()

        for category in VoteCategory.allCategories {
            votes.append(Vote(category: category, talk: self))
        }
    }

    convenience init(session: Session) {
        self.init(uuid: NSUUID().UUIDString, session: session)
    }
   
}
