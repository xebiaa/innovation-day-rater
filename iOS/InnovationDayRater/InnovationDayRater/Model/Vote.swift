//
//  Vote.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

enum VoteCategory: NSInteger {
    case Originality = 1
    case Creativity
    case Realistic
    case Technology

    static let allCategories = [Originality, Creativity, Realistic, Technology]
}

class Vote: NSObject {

    let uuid: String
    var category: VoteCategory
    var value: Int?
    unowned let talk: Talk

    init(category: VoteCategory, value: Int?, uuid: String, talk: Talk) {
        self.uuid = NSUUID().UUIDString
        self.category = category
        self.talk = talk
    }

    convenience init(category: VoteCategory, talk: Talk) {
        self.init(category: category, value: nil, uuid: NSUUID().UUIDString, talk: talk)
    }
}
