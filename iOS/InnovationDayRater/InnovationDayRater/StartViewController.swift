//
//  StartViewController.swift
//  InnovationDayRater
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class StartViewController: UIViewController, UIAlertViewDelegate {

    let model: Model = ServerApi()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let ratingViewController = segue.destinationViewController as? RatingViewController {
            ratingViewController.hostMode = segue.identifier == "Host"
            ratingViewController.model = model
        }
    }

    @IBAction func startHost(sender: AnyObject) {
        let alert = UIAlertView(title: "Host Session", message: "Please fill in a session name", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Continue")
        alert.alertViewStyle = .PlainTextInput
        alert.show()
    }

    @IBAction func join(sender: AnyObject) {
        let alert = UIAlertView(title: "Join Session", message: "Please fill in the session name", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Continue")
        alert.alertViewStyle = .PlainTextInput
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.buttonTitleAtIndex(buttonIndex) == "Continue" {
            if alertView.title == "Host Session" {
                if let name = alertView.textFieldAtIndex(0)?.text where count(name) > 0 {
                    model.createSession(name)
                        .then { session -> Void in
                            self.performSegueWithIdentifier("Host", sender: self)
                    }
                }
            } else {
                if let name = alertView.textFieldAtIndex(0)?.text where count(name) > 0 {
                    model.joinSession(name)
                        .then { session -> Void in
                            self.performSegueWithIdentifier("Join", sender: self)
                    }
                }
            }
        }
    }
}