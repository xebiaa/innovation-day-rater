//
//  StyleKitViews.swift
//  InnovationDayRater
//
//  Created by Jeroen Leenarts on 21-08-15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import FontAwesome_swift

class StarView : UIButton {

    override var selected: Bool {
        didSet {
            if selected {
                starFillColor = RaterStyleKit.starFillColor
            } else {
                starFillColor = UIColor.clearColor()
            }
        }
    }
    
    var starFillColor = UIColor.clearColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var starStrokeColor = RaterStyleKit.starStrokeColor {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        RaterStyleKit.drawRatingStar(frame: rect, starFillParameter: starFillColor, starStrokParameter: starStrokeColor)
    }
}

enum Rating: Int {
    case None
    case One
    case Two
    case Three
    case Four
    case Five
}

protocol RatingViewDelegate {
    func didRate(rating: Rating, ratingView: RatingView)
    func shouldAllowVoting() -> Bool
}

@IBDesignable
class RatingView : UIView {
    
    var delegate: RatingViewDelegate?
    var rating: Rating = .None {
        didSet {
            for index in 0 ..< starViews.count {
                starViews[index].selected = false
            }

            for index in 0 ..< rating.rawValue {
                starViews[index].selected = true
            }
        }
    }

    @IBInspectable
    var starFillColor = RaterStyleKit.starFillColor {
        didSet {
            for index in 0...4 {
                starViews[index].starFillColor = starFillColor
                starViews[index].setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable
    var starStrokeColor = RaterStyleKit.starStrokeColor {
        didSet {
            for index in 0...4 {
                starViews[index].starStrokeColor = starStrokeColor
                starViews[index].setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var ratingLabelText = "Rate me" {
        didSet {
            labelRating.text = ratingLabelText
            updateAccessibilityLabels()
            self.setNeedsLayout()
        }
    }

    private var starViews: [StarView] = []
    private var labelRating = UILabel();
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        buildViewHierarchy()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildViewHierarchy()
    }
    
    func buildViewHierarchy() {

        self.addSubview(labelRating)
        self.labelRating.text = ratingLabelText
        
        for index in 0...4 {
            let starView = StarView()
            self.backgroundColor = UIColor.clearColor()
            self.addSubview(starView)
            starView.backgroundColor = UIColor.clearColor()

            starView.addTarget(self, action: "starSelected:", forControlEvents: UIControlEvents.TouchUpInside)

            self.starViews.append(starView)
        }

        updateAccessibilityLabels()
    }

    func starSelected(sender: StarView) {
        if let delegate = delegate {
            if !delegate.shouldAllowVoting() {
                return
            }
        } else {
            return
        }

        var enable = true
        for starView in starViews {
            starView.selected = enable
            if starView == sender {
                enable = false
            }
        }

        if let findIndex = find(starViews, sender) {
            let intRating = distance(starViews.startIndex, findIndex) + 1
            if let rating = Rating(rawValue: intRating) {
                self.rating = rating
                delegate?.didRate(rating, ratingView: self)
            }
        }
        updateAccessibilityLabels()
    }

    func updateAccessibilityLabels() {
        for index in 0...4 {
            let starView = starViews[index]
            if let labeRatingText = labelRating.text {
                starView.accessibilityLabel = "\(labelRating.text!) \(index + 1) star"
            } else {
                starView.accessibilityLabel = "something \(index + 1) star"
            }
            if starView.selected {
                starView.accessibilityTraits = UIAccessibilityTraitButton | UIAccessibilityTraitSelected
            } else {
                starView.accessibilityTraits = UIAccessibilityTraitButton
            }

        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.labelRating.sizeToFit()
        
        self.labelRating.center = CGPoint(x: self.frame.size.width/2.0, y:self.labelRating.bounds.size.height / 2.0)
        var margin: CGFloat = CGFloat(100 / 5)
        
        var starWidth: CGFloat = (self.bounds.size.width - 100) / 5.0
        if starWidth > 55 {
            starWidth = 55.0
            margin = (self.bounds.size.width - 5.0 * starWidth) / 5.0
        }
        
        let starHeight: CGFloat = starWidth
        
        for index in 0...4 {
            starViews[index].frame = CGRect(x: CGFloat(index) * starWidth + (CGFloat(index) * margin) + margin / 2 , y: self.labelRating.frame.height, width: starWidth, height: starHeight)
        }
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 100 * 5, height: 55 + 20)
    }
}

class FontAwesomeHelper : NSObject {

    private struct Cache {
        static var gearIconTargets: [AnyObject]?
        static var pauseIconTargets: [AnyObject]?
        static var stepForwardIconTargets: [AnyObject]?
    }


    lazy var gearIcon: UIImage = {
        return UIImage.fontAwesomeIconWithName(.Gear, textColor: UIColor.blackColor(), size: CGSizeMake(30, 30))
    }()

    lazy var pauseIcon: UIImage = {
        return UIImage.fontAwesomeIconWithName(.Pause, textColor: UIColor.blackColor(), size: CGSizeMake(30, 30))
    }()

    lazy var stepForwardIcon: UIImage = {
        return UIImage.fontAwesomeIconWithName(.StepForward, textColor: UIColor.blackColor(), size: CGSizeMake(30, 30))
    }()

    @IBOutlet var gearIconTargets: [AnyObject]! {
        get { return Cache.gearIconTargets }
        set {
            Cache.gearIconTargets = newValue
            for target: AnyObject in newValue {
                target.setImage(self.gearIcon)
            }
        }
    }

    @IBOutlet var pauseIconTargets: [AnyObject]! {
        get { return Cache.pauseIconTargets }
        set {
            Cache.pauseIconTargets = newValue
            for target: AnyObject in newValue {
                target.setImage(self.pauseIcon)
            }
        }
    }

    @IBOutlet var stepForwardIconTargets: [AnyObject]! {
        get { return Cache.stepForwardIconTargets }
        set {
            Cache.stepForwardIconTargets = newValue
            for target: AnyObject in newValue {
                target.setImage(self.stepForwardIcon)
            }
        }
    }

}

@objc protocol FontAwesomeHelperSettableImage {
    func setImage(image: UIImage!)
}