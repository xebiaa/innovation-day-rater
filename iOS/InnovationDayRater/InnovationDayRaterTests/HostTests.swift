//
//  HostTests.swift
//  InnovationDayRater
//
//  Created by Stijn Spijker on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import XCTest
import KIF

class HostTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        tester().tapViewWithAccessibilityLabel("hostButton")
        
        let continueButton = tester().waitForViewWithAccessibilityLabel("Continue")!
        
        tester().enterTextIntoCurrentFirstResponder("test")
        
        tester().tapViewWithAccessibilityLabel("Continue")
    }
    
    override func tearDown() {
        super.tearDown()
        
        tester().tapViewWithAccessibilityLabel("backButton")
        
        tester().waitForViewWithAccessibilityLabel("hostButton")
    }
    
    func testHost() {
        
        let timeLabel = tester().waitForViewWithAccessibilityLabel("timeLabel")
        
        XCTAssert(timeLabel != nil, "Rating Window Appeard")
    }
}