//
//  JoinTests.swift
//  InnovationDayRater
//
//  Created by Stijn Spijker on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import XCTest

class JoinTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        tester().tapViewWithAccessibilityLabel("joinButton")
        
        let continueButton = tester().waitForViewWithAccessibilityLabel("Continue")!
        
        tester().enterTextIntoCurrentFirstResponder("test")
        
        tester().tapViewWithAccessibilityLabel("Continue")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        tester().tapViewWithAccessibilityLabel("backButton")
        
    }
    
    func testJoin() {
        
        
        let timeLabel = tester().waitForViewWithAccessibilityLabel("timeLabel")
        
        XCTAssert(timeLabel != nil, "Rating Window Appeard")
    }
}
