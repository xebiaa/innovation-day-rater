//
//  InnovationDayRaterTests.swift
//  InnovationDayRaterTests
//
//  Created by Lammert Westerhoff on 21/08/15.
//  Copyright (c) 2015 Xebia Nederland B.V. All rights reserved.
//

import UIKit
import XCTest

class RatingTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Host a new session to test.
        tester().tapViewWithAccessibilityLabel("hostButton")
        let continueButton = tester().waitForViewWithAccessibilityLabel("Continue")!
        tester().enterTextIntoCurrentFirstResponder("test")
        tester().tapViewWithAccessibilityLabel("Continue")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        tester().tapViewWithAccessibilityLabel("backButton")
    }
    
    func testRatings() {
        
        tester().waitForViewWithAccessibilityLabel("Originality 1 star") as! UIButton
        
        tester().tapViewWithAccessibilityLabel("Originality 1 star");
        tester().tapViewWithAccessibilityLabel("Creativity 5 star");
        tester().tapViewWithAccessibilityLabel("Realistic 5 star");
        tester().tapViewWithAccessibilityLabel("Technology 4 star");
        
        // Check if originality is 1
        var button = tester().waitForViewWithAccessibilityLabel("Originality 1 star") as! UIButton
        XCTAssert(button.selected, "Originality")
        
        // Check if originality is not 2
        button = tester().waitForViewWithAccessibilityLabel("Originality 2 star") as! UIButton;
        XCTAssert(button.selected == false, "Originality")
        
        // Check creativity is 5
        button = tester().waitForViewWithAccessibilityLabel("Creativity 5 star") as! UIButton;
        XCTAssert(button.selected, "Creativity")
        
        // Check realistic is 5
        button = tester().waitForViewWithAccessibilityLabel("Realistic 5 star") as! UIButton;
        XCTAssert(button.selected, "Realistic")
        
        // Check if Technology is 3
        button = tester().waitForViewWithAccessibilityLabel("Technology 3 star") as! UIButton
        XCTAssert(button.selected == true, "Technology")
        
        // Check if Technology is 4
        button = tester().waitForViewWithAccessibilityLabel("Technology 4 star") as! UIButton;
        XCTAssert(button.selected, "Technology")
        
        // Check if Technology is not 5
        button = tester().waitForViewWithAccessibilityLabel("Technology 5 star") as! UIButton;
        XCTAssert(button.selected == false, "Technology")
    }
}
