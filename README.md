Xebia Innovation Day Rating App

Booing boring people off the stage the modern way (tm).

# Technology Stack

- [iOS](https://www.apple.com/nl/ios/)
- [Android](https://www.android.com/)
- [Nearby](https://developers.google.com/nearby/)

# General Idea

Put a presenter on the stage. Run a timer for five minutes. This is all the
time the speaker has for their talk. The audience can use their phones to boo
(or woo) the speaker, making the speaker's time count down faster, or slower.
Sound an alarm on all devices simultaneously when the time runs out. Bonus
objective: have some sort of cool visualization of how the speaker is doing.

# Server

Install the [Google App Engine SDK for Go](https://cloud.google.com/appengine/downloads?hl=en_US#Google_App_Engine_SDK_for_Go) and put the binaries on your PATH.

Initialize the project for local build:

    $ export GOPATH=$PWD
    $ goapp get google.golang.org/appengine
    $ goapp get github.com/gorilla/mux

Run project locally:

    $ goapp serve Server

This defaults to the loopback adapter. If you want to listen on all interfaces,
throw a `-host 0.0.0.0` in there.

Deploy the application using:

    goapp deploy -oauth -application xebia-innovation-day-rater Server

## API

Implemented in [server.go](https://github.com/xebia/innovation-day-rater/blob/master/Server/server.go)

### Data

Session:

    {
        "SessionID" : "UUID"
    ,   "Name" : "Name"
    ,   "Talks" : [talk]
    }

 Talk:

    {
        "TalkID" : "UUID"
    ,   "Name" : "Name"
    ,   "Votes" : [vote]
    }

Vote:

    {
        "VoteID" : "UUID"
    ,   "UserID" : "UUID"
    ,   "VoteCategory" : 1-4
    ,   "Value" : 1-5
    ,   "Timestamp" : Time (RFC 3339) (generated server-side)
    }

### Methods / Endpoints

| Method | Path                                               | Body | Response        |
| ------ | -------------------------------------------------- | ---- | --------------- |
| POST   | /session                                           | nil  | session         |
| GET    | /session?name=sessionName                          | nil  | session         |
| GET    | /session/{sessionID}                               | nil  | session         |
| POST   | /session/{sessionID}/talk                          | talk | [talk]          |
| GET    | /session/{sessionID}/talk/current                  | nil  | talk  (current) |
| GET    | /session/{sessionID}/talk/{talkID}                 | nil  | talk            |
| POST   | /session/{sessionId}/talk/{talkId}/vote            | vote | vote            |
| GET    | /session/{sessionId}/talk/{talkId}/vote            | nil  | [vote]          |

# Copyright and License

Copyright 2015 Xebia Nederland B.V.

Creative Commons [Attribution-NonCommercial 4.0 International][cc-by-nc]

[cc-by-nc]:http://creativecommons.org/licenses/by-nc/4.0/
