package hello

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"net/http"
	"time"
)

type session struct {
	SessionID string
	Name      string
	Talks     []talk
}

type talk struct {
	TalkID string
	Name   string
	Votes  []vote
}

type vote struct {
	VoteID       string
	UserID       string
	VoteCategory int // 1-4
	Value        int // 1-5
	Timestamp    time.Time
}

var router = mux.NewRouter()
var sessions []session
var currentTalkIndex = -1

func sessionIndexByGuid(guid string) int {
	for index, s := range sessions {
		if s.SessionID == guid {
			return index
		}
	}
	return -1
}

func sessionIndexByName(name string) int {
	for index, s := range sessions {
		if s.Name == name {
			return index
		}
	}
	return -1
}

func sessionFromRequest(r *http.Request) int {
	pathParams := mux.Vars(r)
	if sid, ok := pathParams["sessionId"]; ok {
		return sessionIndexByGuid(sid)
	}

	sessionName := r.FormValue("name")
	if sessionName != "" {
		return sessionIndexByName(sessionName)
	}

	return -1
}

func corsHandler(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Add("Access-Control-Allow-Headers", "content-type")
		if r.Method != "OPTIONS" {
			h.ServeHTTP(w, r)
		}
	}
}

func init() {
	sessions = []session{}
	http.Handle("/", corsHandler(router))

	router.HandleFunc("/session", sessionHandler)
	router.HandleFunc("/session/{sessionId}", sessionHandler)
	router.HandleFunc("/session/{sessionId}/talk", createOrGetTalkHandler)
	router.HandleFunc("/session/{sessionId}/talk/{talkId}", getTalkHandler)
	router.HandleFunc("/session/{sessionId}/talk/{talkId}/vote", voteHandler)
}

func sessionHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	if r.Method == "POST" {
		// create new Session
		var newSession session
		parseError := json.NewDecoder(r.Body).Decode(&newSession)
		if parseError != nil {
			log.Debugf(ctx, "Error parsing session: %s", parseError.Error())
			writeError(http.StatusBadRequest, "Unable to parse JSON: "+parseError.Error(), w)
		} else {
			if newSession.Talks == nil {
				newSession.Talks = []talk{}
			}
			sessions = append(sessions, newSession)
			marshalled, _ := json.Marshal(newSession)
			w.Header().Set("Content-Type", "application/json")
			w.Write(marshalled)
		}

	} else if r.Method == "GET" {
		sessionIndex := sessionFromRequest(r)
		if sessionIndex == -1 {
			writeError(404, "Session not found", w)
			return
		}

		marshalled, _ := json.Marshal(sessions[sessionIndex])
		w.Header().Set("Content-Type", "application/json")
		w.Write(marshalled)
	} else {
		writeError(405, "Method not allowed", w)
	}
}

func createOrGetTalkHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	sessionIndex := sessionFromRequest(r)
	if sessionIndex == -1 {
		writeError(500, "No sessionId path parameter or name query parameter passed", w)
		return
	}

	if r.Method == "GET" {
		marshalled, _ := json.Marshal(sessions[sessionIndex].Talks)
		w.Header().Set("Content-Type", "application/json")
		w.Write(marshalled)
		return
	}

	if r.Method == "POST" {
		var newTalk talk
		parseError := json.NewDecoder(r.Body).Decode(&newTalk)

		if parseError != nil {
			log.Debugf(ctx, "Error parsing session: %s", parseError.Error())
			writeError(http.StatusBadRequest, "Unable to parse JSON: "+parseError.Error(), w)
		} else {
			newTalk.Votes = []vote{}
			sessions[sessionIndex].Talks = append(sessions[sessionIndex].Talks, newTalk)
			currentTalkIndex = len(sessions[sessionIndex].Talks) - 1
			marshalled, _ := json.Marshal(newTalk)
			w.Header().Set("Content-Type", "application/json")
			w.Write(marshalled)
		}
		return
	}

	writeError(405, "Method not allowed", w)
}

func talkIndexByGuid(guid string, session session) int {
	for index, t := range session.Talks {
		if t.TalkID == guid {
			return index
		}
	}
	return -1
}

func talkIndexByRequest(r *http.Request) (int, int) {
	sessionIndex := sessionFromRequest(r)
	if sessionIndex != -1 {
		if tid, ok := mux.Vars(r)["talkId"]; ok {
			if tid == "current" && currentTalkIndex != -1 {
				return sessionIndex, currentTalkIndex
			}
			return sessionIndex, talkIndexByGuid(tid, sessions[sessionIndex])
		}
	}
	return -1, -1
}

func getTalkHandler(w http.ResponseWriter, r *http.Request) {
	sessionIndex, talkIndex := talkIndexByRequest(r)
	if sessionIndex == -1 || talkIndex == -1 {
		writeError(404, "Talk not found", w)
		return
	}

	marshalled, _ := json.Marshal(sessions[sessionIndex].Talks[talkIndex])
	w.Header().Set("Content-Type", "application/json")
	w.Write(marshalled)
}

func voteHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	sessionIndex, talkIndex := talkIndexByRequest(r)
	if sessionIndex == -1 || talkIndex == -1 {
		writeError(404, "Talk not found", w)
		return
	}

	if r.Method == "POST" {
		var newVote vote
		parseError := json.NewDecoder(r.Body).Decode(&newVote)
		if parseError != nil {
			log.Debugf(ctx, "Error parsing session: %s", parseError.Error())
			writeError(http.StatusBadRequest, "Unable to parse JSON: "+parseError.Error(), w)
		} else {
			newVote.Timestamp = time.Now()
			sessions[sessionIndex].Talks[talkIndex].Votes = append(sessions[sessionIndex].Talks[talkIndex].Votes, newVote)
		}

		return
	}

	if r.Method == "GET" {
		marshalled, _ := json.Marshal(sessions[sessionIndex].Talks[talkIndex].Votes)
		w.Header().Set("Content-Type", "application/json")
		w.Write(marshalled)
		return
	}

	writeError(405, "Method not allowed", w)
}

func writeError(code int, error string, w http.ResponseWriter) {
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(error)
}
